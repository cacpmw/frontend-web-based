# **NEPPO - Teste Frontend Web Based** #

Este projeto foi desenvolvido em abril de 2018 por Carlos Albuquerque Carneiro como 
parte do processo seletivo da Neppo (Uberlandia-MG). O projeto contêm uma api e um 
cliente e foi desenvolvido em PHP utilizando o framework Laravel 5.3 no backend e 
o sbadmin 2 no frontend (contêm bootstrap, js etc...). Todo o código foi escrito 
em inglês por ser mais fácil trabalhar com o framework neste idioma.

#Desenvolvimento:
Foram utilizadas técnicas de padrão de projetos (design patterns) que compõem o 
S.O.L.I.D (interfaces, injeção de dependências etc...) e arquitetura RESTful.

O Layout foi desenvolvido de maneira modular,responsiva e SPA.

#Funcionalidades:
Todas as funcionalidades solicitadas foram implementadas

#Tecnologias utilizadas:
* Laravel 5.3 através do composer
* Sbadmin 2 (bootstrap, fontawesome, css3 etc...) através do npm
* Charts.Js para fazer o gráfico

#Ferramentas:
* PhpStorm na versão mais recente
* Postman na versão mais recente
* Navegadores Opera, Chrome e FireFox nas versões mais recentes
* Xampp (apache e mysql) versões mais recentes
* Linux Mint

#Testes:
* Testes Unitários na API
* Testes de performance nos navegadores.

Os testes de performance foram realizados em 3 modalidades:
* Online
* Mid-Tier Mobile (Fast 3g & 4x CPU slowdown)
* Low-Tier Mobile (Slow 3g & 6x CPU slowdown)
 
 _Online_:
* Tempo de carregamento: ~700ms
* Total de requisições: 12
* KBs transferidos: 32
* Tempo total de processamento 1 s

_Mid-Tier Mobile_:
* Tempo de carregamento: 2.32s
* Total de requisições: 12
* KBs transferidos: 32
* Tempo total de processamento 3.67 s
 
_Low-Tier Mobile_:
* Tempo de carregamento: 6 s
* Total de requisições: 12
* KBs transferidos: 32
* Tempo total de processamento 10 s
 
#Notas:

Como eu informei na entrevista, front-end não é a minha principal habilidade
espero ter atendido as expectativas. Desde já agradeço a 
oportunidade.