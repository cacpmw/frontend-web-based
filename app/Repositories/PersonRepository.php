<?php

namespace Repositories;

use App\Http\Resources\PersonCollection;
use App\Http\Resources\PersonResource;
use App\Person;
use Interfaces\iCrud; //adicionei caminho da pasta interface no composer.json e dei comando <composer dump-autoload> no cmd


class PersonRepository implements iCrud
{

    public function all()
    {
        return PersonCollection::collection(Person::all());
    }

    public function store($r)
    {
        $person = new Person();
        $person->name = $r->nome;
        $person->ssn = $r->cpf;
        $person->gender = $r->genero;
        $person->address = $r->endereco;
        $person->birthday = $r->nascimento;
        $person->save();
        return new PersonResource($person);
    }

    public function update($r, $object)
    {
        $r['name'] = $r->nome;
        $r['ssn'] = $r->cpf;
        $r['gender'] = $r->genero;
        $r['address'] = $r->endereco;
        $r['birthday'] = $r->nascimento;
        unset($r['nome']);
        unset($r['cpf']);
        unset($r['genero']);
        unset($r['endereco']);
        unset($r['nascimento']);
        $object->fill($r->all());
        $object->save();

        return new PersonResource($object);
    }

    public function destroy($object)
    {
        $object->delete();
    }

    public function show($object)
    {
        return new PersonResource($object);

    }

    public function find($id)
    {
        return new PersonResource(Person::find($id));
    }


}