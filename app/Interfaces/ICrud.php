<?php

namespace Interfaces;

use Illuminate\Http\Request;

interface iCrud
{

    public function all();

    public function store($r);

    public function update($r, $object);

    public function destroy($object);

    public function show($object);

    public function find($id);
}