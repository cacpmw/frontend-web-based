<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePerson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $elements = explode('/', $this->getRequestUri());

        return [
            'nome' => 'required|max:191|string',
            'cpf' => 'required|string|max:11|unique:people,ssn,' . end($elements),
            'nascimento' => 'required|date',
            'endereco' => 'nullable|string|max:191',
            'genero' => 'required|string|max:191'
        ];
    }
}
