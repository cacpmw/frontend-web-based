<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class ClientPersonController extends Controller
{
    public function index()
    {
        try {
            $client = new Client();
            $res = $client->get(route('persons.index'));
            $persons = json_decode($res->getBody()->getContents());
            $men = 0;
            $women = 0;
            $ages = [
                0 => 0, //0 a 9
                1 => 0, //10 a 19
                2 => 0, //20 a 29
                3 => 0, //30 a 39
                4 => 0  //Maior que 40
            ];
            foreach ($persons as $person) {
                if ($person->genero == 'Masculino') {
                    $men++;
                } else {
                    $women++;
                }
                $age = Carbon::parse($person->nascimento)->age;
                if ($age >= 0 && $age <= 9) {
                    $ages[0]++;
                } else if ($age >= 10 && $age <= 19) {
                    $ages[1]++;
                } else if ($age >= 20 && $age <= 29) {
                    $ages[2]++;
                } else if ($age >= 30 && $age <= 39) {
                    $ages[3]++;
                } else {
                    $ages[4]++;
                }

            }

            return view('persons.index', ['persons' => $persons, 'men' => $men, 'women' => $women, 'ages' => $ages]);
        } catch (\Exception $e) {
            return redirect()->action('ClientPersonController@index')->with('msg', 'Não foi possivel executar a ação');
        }

    }


    public function create()
    {
        return redirect()->action('ClientPersonController@index');
    }

    public function store(Request $r)
    {
        try {
            unset($r['_method']);
            unset($r['_token']);
            $endpoint = url('/') . '/api/persons';
            $options = [
                'headers' => ['Content-Type' => 'application/json',
                    'Accept' => 'application/json'],
                'json' => ['nome' => $r->nome,
                    'cpf' => $r->cpf,
                    'endereco' => $r->endereco,
                    'genero' => $r->genero,
                    'id' => $r->id,
                    'nascimento' => $r->nascimento],

            ];

            $client = new Client();
            $client->post($endpoint, $options);
            return redirect()->action('ClientPersonController@index');
        } catch (\Exception $e) {
            return redirect()->action('ClientPersonController@index')->with('msg', 'Não foi possivel executar a ação');
        }

    }


    public function edit(Request $r)
    {
        return redirect()->action('ClientPersonController@index');

    }

    public function update(Request $r)
    {
        try {

            unset($r['_method']);
            unset($r['_token']);
            $endpoint = url('/') . '/api/persons/' . $r->id;
            $options = [
                'headers' => ['Content-Type' => 'application/json',
                    'Accept' => 'application/json'],
                'json' => ['nome' => $r->nome,
                    'cpf' => $r->cpf,
                    'endereco' => $r->endereco,
                    'genero' => $r->genero,
                    'id' => $r->id,
                    'nascimento' => $r->nascimento],

            ];

            $client = new Client();
            $client->put($endpoint, $options);

            return redirect()->action('ClientPersonController@index');
        } catch (ClientException $e) {

            return redirect()->action('ClientPersonController@index')->with('msg', 'Não foi possivel executar a ação');
        }

    }

    public function destroy(Request $r)
    {
        try {
            $client = new Client();
            $client->delete($r->href);

            return redirect()->action('ClientPersonController@index');
        } catch (\Exception $e) {
            return redirect()->action('ClientPersonController@index')->with('msg', 'Não foi possivel executar a ação');
        }


    }
}
