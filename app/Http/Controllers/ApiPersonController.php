<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePerson;
use App\Http\Requests\UpdatePerson;
use App\Http\Resources\PersonResource;
use App\Person;
use Repositories\PersonRepository as ICrud;
use Symfony\Component\HttpFoundation\Response;

class ApiPersonController extends Controller
{
    protected $persons; //Repository

    public function __construct(ICrud $p)
    {
        $this->persons = $p;
    }


    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|Response
     */
    public function index()
    {
        try {
            return json_encode($this->persons->all());
        } catch (\Exception $e) {

            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);

        }

    }

    /**
     * @param StorePerson $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(StorePerson $request)
    {
        try {

            $return = $this->persons->store($request);
            if (isset($return)) {
                return response(['data' => $return], Response::HTTP_CREATED);
            } else {
                return response(null, Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);

        }


    }

    /**
     * @param Person $person
     * @return PersonResource
     */
    public function show(Person $person)
    {
        try {
            return json_encode($this->persons->show($person));
        } catch (\Exception $e) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);

        }


    }

    /**
     * @param UpdatePerson $request
     * @param Person $person
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(UpdatePerson $request, Person $person)
    {
        try {
            $return = $this->persons->update($request, $person);
            if (isset($return)) {
                return response(['data' => $return], Response::HTTP_CREATED);
            } else {
                return response(null, Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }


    }

    public function find($id)
    {
        try {
            $person = $this->persons->find($id);


            if (!isset($person)) {
                return response(null, Response::HTTP_NOT_FOUND);
            } else {
                return json_encode($person);
            }

        } catch (\Exception $e) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function destroy(Person $person)
    {
        try {
            $this->persons->destroy($person);
            return response(null, Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
}
