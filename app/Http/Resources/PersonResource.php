<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PersonResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nome'=>$this->name,
            'nascimento'=>$this->birthday,
            'cpf'=>$this->ssn,
            'endereco'=>$this->address,
            'genero'=>$this->gender,
            'href'=> route('persons.show',$this->id),
        ];
    }
}
