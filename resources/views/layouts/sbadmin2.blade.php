<!DOCTYPE html>
<html lang="en">
@include('partials._head')

<body>
<div id="wrapper">
    @include('partials._navbar')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="content-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

@include('partials._bodyScripts')

</body>

</html>
