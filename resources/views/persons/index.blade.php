@extends('layouts.sbadmin2')

@section('content')

    @if(session('msg'))
        <div class="alert alert-danger">
            <ul>
                <li>{{session('msg')}}</li>
            </ul>
        </div>
    @endif
    <div class="row">

        @include('partials._dashboardCards')
    </div>
    <div class="row">
        @if(!count($persons)<1)
            <div class="col-md-6">
                <div class="row">
                    @include('partials._dashboardPersonTable')
                </div>
            </div>
            <div class="col-md-6">
                @include('partials._dashboardDonutChart')
            </div>
        @endif

    </div>
    @include('partials._dashboardCardNewPersonModal')
    @include('partials._dashboardMyModalEdit')
    @include('partials._dashboardMyModalDelete')
    @include('partials._dashboardMyModalShow')
    @include('partials._dashboardJs')

@endsection

