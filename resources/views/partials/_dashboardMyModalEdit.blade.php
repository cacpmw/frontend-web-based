<div id="myModalEdit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar</h4>
            </div>
            <div class="modal-body">
                <form id="modalEdit" action="" method="POST" role="form">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="id-edit" value="">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nome-edit">Nome</label>
                                <input name="nome" id="nome-edit" maxlength="191" required class="form-control"
                                       value="">

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cpf-edit">CPF</label>
                                <input name="cpf" type="number" id="cpf-edit" oninput="javascript: if (this.value.length > 11)
                    this.value = this.value.slice(0, 11);" required class="form-control"
                                       value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="genero-edit">Gênero</label>
                                <select class="form-control" required name="genero" id="genero-edit">
                                    <option value="">Selecione
                                    <option value="Feminino">Feminino</option>
                                    <option value="Masculino">Masculino</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="endereco-edit">Endereço</label>
                                <input name="endereco" maxlength="191" id="endereco-edit" class="form-control"
                                       value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nascimento-edit">Nascimento</label>
                                <input name="nascimento" id="nascimento-edit" type="date" required
                                       class="form-control"
                                       value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">Atualizar</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>

    </div>
</div>