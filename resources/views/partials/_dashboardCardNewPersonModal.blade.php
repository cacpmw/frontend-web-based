<div id="myModalPerson" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nova Pessoa</h4>
            </div>
            <div class="modal-body">
                @include('partials._dashboardNewPersonForm')
            </div>
        </div>

    </div>
</div>