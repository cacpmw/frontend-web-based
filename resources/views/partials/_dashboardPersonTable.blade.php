<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>

            <th>Nome</th>
            <th>Cpf</th>
            <th>Genero</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>

        @foreach($persons as $person)
            <tr>

                <td>{{$person->nome}}</td>
                <td>{{$person->cpf}}</td>
                <td>{{$person->genero}}</td>
                <td>
                    <a href="" data-toggle="modal" data-target="#myModalShow"
                       onclick="getPerson({{$person->id}})"><i title="Ver"
                                                               class="fa fa-eye"></i></a>

                    <a href=""
                       data-toggle="modal" data-target="#myModalEdit"
                       onclick="editPerson({{$person->id}})">
                        <i title="Editar"
                           class="fa fa-edit "></i>
                    </a>

                    <a href="" data-toggle="modal" data-target="#myModalDelete"
                       onclick="destroyPerson({{$person->id}})"><i
                                title="Deletar" class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>