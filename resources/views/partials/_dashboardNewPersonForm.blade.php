<form action="{{route('client.store')}}" method="POST" role="form">
    {{method_field('POST')}}
    {{csrf_field()}}
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label for="nome">Nome</label>
                <input name="nome" maxlength="191" required class="form-control">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label for="cpf">CPF</label>
                <input name="cpf" type="number" oninput="javascript: if (this.value.length > 11)
                    this.value = this.value.slice(0, 11);" required class="form-control">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label for="genero">Gênero</label>
                <select class="form-control" required name="genero" id="genero">
                    <option value="">Selection</option>
                    <option value="Feminino">Feminino</option>
                    <option value="Masculino">Masculino</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label for="endereco">Endereço</label>
                <input name="endereco" maxlength="191" class="form-control">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <label for="nascimento">Nascimento</label>
                <input name="nascimento" type="date" required class="form-control">
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block">Salvar</button>
            </div>
        </div>

    </div>

</form>
