<script>
    //bad performance since I already have all the data on the view
    /*function getPersonAjax(href) {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', href);
        xhr.onload = function () {
            if (xhr.status === 200) {
                person = JSON.parse(xhr.responseText)

                var personIdElement = document.getElementById('person-id-show');
                var personNameElement = document.getElementById('person-name-show');
                var personSsnElement = document.getElementById('person-ssn-show');
                var personGenderElement = document.getElementById('person-gender-show');
                var personBirthdayElement = document.getElementById('person-birthday-show');
                var personAddressElement = document.getElementById('person-address-show')

                personIdElement.innerHTML = person.id;
                personNameElement.innerHTML = person.nome;
                personSsnElement.innerHTML = person.cpf;
                personGenderElement.innerHTML = person.genero;
                personBirthdayElement.innerHTML = person.nascimento;
                personAddressElement.innerHTML = person.endereco;
            }
            else {
                alert('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();

    }*/

    function destroyPerson(personId) {
        var persons ={!! json_encode($persons) !!};
        var person = null;

        for (var i = 0; i < persons.length; i++) {
            if (persons[i].id === personId) {
                person = persons[i];
                break;
            }
        }


        document.getElementById("person-id-delete").innerHTML = "<strong>Id: </strong>" + person.id;
        document.getElementById("person-name-delete").innerHTML = "<strong>Nome: </strong>" + person.nome;
        document.getElementById("person-ssn-delete").innerHTML = "<strong>CPF: </strong>" + person.cpf;
        document.getElementById("person-gender-delete").innerHTML = "<strong>Gênero: </strong>" + person.genero;
        document.getElementById("person-birthday-delete").innerHTML = "<strong>Nascimento: </strong>" + person.nascimento;
        document.getElementById("person-address-delete").innerHTML = "<strong>Endereço: </strong>" + person.endereco;


        document.getElementById("modalForm").action = '{!! url('/').'/' !!}' + 'client/' + person.id;
        document.getElementById("hrefInputHidden").value = person.href;
    }

    function getPerson(personId) {
        var persons ={!! json_encode($persons) !!};
        var person = null;

        for (var i = 0; i < persons.length; i++) {
            if (persons[i].id === personId) {
                person = persons[i];
                break;
            }
        }
        document.getElementById('person-name-show').innerHTML = "<strong>Id: </strong>" + person.id;
        document.getElementById('person-name-show').innerHTML = "<strong>Nome: </strong>" + person.nome;
        document.getElementById('person-ssn-show').innerHTML = "<strong>CPF: </strong>" + person.cpf;
        document.getElementById('person-gender-show').innerHTML = "<strong>Gênero: </strong>" + person.genero;
        document.getElementById('person-birthday-show').innerHTML = "<strong>Nascimento: </strong>" + person.nascimento;
        document.getElementById('person-address-show').innerHTML = "<strong>Endereço: </strong>" + person.endereco;


    }

    function editPerson(personId) {
        var persons ={!! json_encode($persons) !!};
        var person = null;

        for (var i = 0; i < persons.length; i++) {
            if (persons[i].id === personId) {
                person = persons[i];
                break;
            }
        }
        debugger;
        document.getElementById('nome-edit').value = person.nome;
        document.getElementById('cpf-edit').value = person.cpf;
        document.getElementById('endereco-edit').value = person.endereco;
        document.getElementById('nascimento-edit').value = person.nascimento;
        document.getElementById('id-edit').value = person.id;
        document.getElementById("modalEdit").action = '{!! url('/').'/' !!}' + 'client/' + person.id;

        var optionElement = document.getElementById('genero-edit');
        for (var i = 0; i < 4; i++) {
            if (optionElement.options[i].value === person.genero) {
                optionElement.options[i].selected = true;
                break;
            }
        }

    }
</script>