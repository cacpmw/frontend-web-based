<div id="myModalDelete" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Deseja realmente excluir esta pessoa?</h4>
            </div>
            <div class="modal-body">
                <p id="person-id-delete"></p>
                <p id="person-name-delete"></p>
                <p id="person-ssn-delete"></p>
                <p id="person-gender-delete"></p>
                <p id="person-birthday-delete"></p>
                <p id="person-address-delete"></p>

            </div>
            <div class="modal-footer">
                <form id="modalForm" action="{{url('/')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <input type="hidden" id="hrefInputHidden" name="href" value="">
                    <button type="submit" class="btn btn-danger btn-block">
                        Deletar
                    </button>
                </form>


            </div>
        </div>

    </div>
</div>