<div id="myModalShow" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pessoa</h4>
            </div>
            <div class="modal-body">
                <p id="person-id-show"></p>
                <p id="person-name-show"></p>
                <p id="person-ssn-show"></p>
                <p id="person-gender-show"></p>
                <p id="person-birthday-show"></p>
                <p id="person-address-show"></p>
            </div>
        </div>

    </div>
</div>