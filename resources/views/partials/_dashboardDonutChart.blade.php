@include('partials._chartsJs')
<div class="well well-sm">
    <canvas id="personsByAge" ></canvas>
</div>

<script>
    personsByAge();

    function personsByAge() {

        var ages = {!! json_encode($ages) !!};


        var labels = ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"];
        var data = [];

        for (var i = 0; i < ages.length; i++) {
            data.push(ages[i]);
        }


        var ctx = document.getElementById("personsByAge").getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Idades',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 0, 0,1)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Demonstrativo de idades'
                },
                responsive: true,

            }
        });
    }
</script>