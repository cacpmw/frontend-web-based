<!-- Bootstrap Core CSS -->
<link href="{{asset('startbootstrap-sb-admin-2/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{asset('startbootstrap-sb-admin-2/vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{asset('startbootstrap-sb-admin-2/dist/css/sb-admin-2.css')}}" rel="stylesheet">

{{-- <!-- Morris Charts CSS -->
 <link href="{{asset('startbootstrap-sb-admin-2/vendor/morrisjs/morris.css')}}" rel="stylesheet">--}}

<!-- Custom Fonts -->
<link href="{{asset('startbootstrap-sb-admin-2/vendor/font-awesome/css/font-awesome.min.css')}}"
      rel="stylesheet"
      type="text/css">