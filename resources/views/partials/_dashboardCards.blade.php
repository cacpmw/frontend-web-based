<div class=" col-md-4">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-comments fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{count($persons)}}</div>
                    <div>Pessoas</div>
                </div>
            </div>
        </div>
        <a href="#" data-toggle="modal" data-target="#myModalPerson">
            <div class="panel-footer">
                <span class="pull-left">Nova Pessoa</span>
                <span class="pull-right"><i class="fa fa-plus-circle"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>

</div>
<div class="col-md-4">
    <div class="panel panel-green">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-tasks fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$men}}</div>
                    <div>Homens</div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-md-4">
    <div class="panel panel-red">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-tasks fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$women}}</div>
                    <div>Mulheres</div>
                </div>
            </div>
        </div>

    </div>
</div>

{{--
<div class="col-lg-3 col-md-6">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-tasks fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$undefined}}</div>
                    <div>Indefinidos</div>
                </div>
            </div>
        </div>

    </div>
</div>--}}
