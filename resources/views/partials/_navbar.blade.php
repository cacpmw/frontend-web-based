<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{route('client.index')}}">Neppo</a>
    </div>
    <!-- /.navbar-header -->


    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a href="{{route('client.index')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>


            </ul>
        </div>

    </div>
    <div class="text-center">
        <a href="https://bitbucket.org/cacpmw/frontend-web-based/src/master/" target="_blank"
           class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a>

        <a href="https://www.facebook.com/profile.php?id=100005799405515" target="_blank"
           class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>

        <a href="https://github.com/cacpmw" target="_blank" class="btn btn-social-icon btn-github"><i class="fa fa-github"></i></a>

        <a href="https://www.instagram.com/cacpmw/" target="_blank" class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
        <a href="https://www.linkedin.com/in/carlos-albuquerque-carneiro-5220398a/" target="_blank" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>


    </div>
</nav>