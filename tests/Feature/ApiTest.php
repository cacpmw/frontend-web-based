<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    public function testGetRequest()
    {
        $characters = '0123456789';
        $id = '';
        for ($i = 0; $i < 2; $i++) {
            $id .= $characters[rand(0, 9)];
        }

        //list
        $response = $this->get(url('/') . '/api/persons');
        if ($response->status() == 200) {
            $response->assertStatus(200);
        } else {

            $response->assertStatus(500);

        }
        $response = $this->get(url('/') . '/api/persons/' . $id);
        if ($response->status() == 200) {
            $response->assertStatus(200);
        } else if ($response->status() == 404) {
            $response->assertStatus(404);

        } else {

            $response->assertStatus(500);

        }


    }

    public function testDeleteRequest()
    {

        $response = $this->delete(url('/') . '/api/persons/2');
        if ($response->status() == 204) {
            $response->assertStatus(204);
        } else if ($response->status() == 404) {
            $response->assertStatus(404);
        } else {
            $response->assertStatus(500);
        }


    }

    public function testPostRequest()
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 11; $i++) {
            $randomString .= $characters[rand(0, 9)];
        }

        $endpoint = url('/') . '/api/persons';
        $data = ['nome' => 'Api Post Unit Testing',
            'cpf' => $randomString,
            'genero' => 'Masculino',
            'nascimento' => '1998-9-22',
            'endereco' => null,
        ];
        $headers = ['Content-Type' => 'application/json',
            'Accept' => 'application/json'];

        $response = $this->postJson($endpoint, $data, $headers);

        if ($response->status() == 201) {
            $response->assertStatus(201);
        } else if ($response->status() == 404) {
            $response->assertStatus(404);
        } else if ($response->status() == 422) {
            $response->assertStatus(422);
        } else {
            $response->assertStatus(500);
        }

    }

    public function testPutRequest()
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 11; $i++) {
            $randomString .= $characters[rand(0, 9)];
        }

        $endpoint = url('/') . '/api/persons/23';
        $data = [
            'id' => '23',
            'nome' => 'Api Put Unit Testing',
            'cpf' => $randomString,
            'genero' => 'Feminino',
            'nascimento' => '1998-9-22',
            'endereco' => null,
        ];
        $headers = ['Content-Type' => 'application/json',
            'Accept' => 'application/json'];

        $response = $this->putJson($endpoint, $data, $headers);

        if ($response->status() == 201) {
            $response->assertStatus(201);
        } else if ($response->status() == 404) {
            $response->assertStatus(404);
        } else if ($response->status() == 422) {
            $response->assertStatus(422);
        } else {
            $response->assertStatus(500);
        }
    }
}
